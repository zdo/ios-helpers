//
//  RoundedButton.swift
//  iOS Helpers
//
//  Created by Dmitry Zganyaiko on 27/10/2018.
//  Copyright © 2018 Dmitry Zganyaiko. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
public class RoundedButton : UIButton {
    @IBInspectable
    public var cornerRadius: CGFloat = 2.0 {
        didSet {
            self.clipsToBounds = true
            self.layer.cornerRadius = self.cornerRadius
        }
    }
}
