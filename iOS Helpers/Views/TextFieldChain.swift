//
//  TextFieldChain.swift
//  iOS Helpers
//
//  Created by Dmitry Zganyaiko on 27/10/2018.
//  Copyright © 2018 Dmitry Zganyaiko. All rights reserved.
//

import Foundation
import UIKit

public class TextFieldChain : NSObject {
    private(set) var textFields = [UIView]()
    private(set) var delegates = [UIView: Any]()

    private var onFinish: (() -> Void)?
    internal weak var parentScrollView: UIScrollView?

    public init(textFields: [UIView?], onFinish: (() -> Void)? = nil) {
        super.init()

        self.onFinish = onFinish
        for v in textFields {
            guard let v = v else {
                return
            }

            self.textFields.append(v)
        }

        for v in textFields {
            guard let v = v else {
                return
            }

            if let t = v as? UITextField {
                if let d = t.delegate {
                    self.delegates[v] = d
                }
                t.delegate = self

                if self.textFields.last == t {
                    t.returnKeyType = UIReturnKeyType.continue
                } else {
                    t.returnKeyType = UIReturnKeyType.next
                }
            } else if let t = v as? UITextView {
                if let d = t.delegate {
                    self.delegates[v] = d
                }
                t.delegate = self

                t.returnKeyType = UIReturnKeyType.default
            }
        }

        NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillShowNotification, object: nil, queue: nil) { [weak self] (n) in
            guard let self = self else {
                return
            }

            for v in self.textFields {
                if v.isFirstResponder, let t = v as? UITextView {
                    self.scrollToCursor(textView: t)
                }
            }
        }
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    fileprivate func shouldReturn(view: UIView) -> Bool {
        guard let index = self.textFields.firstIndex(of: view) else {
            return true
        }

        let nextIndex = index + 1
        if nextIndex < self.textFields.count {
            self.textFields[nextIndex].becomeFirstResponder()

            if let textView = self.textFields[nextIndex] as? UITextView {
                self.scrollToCursor(textView: textView)
            }
        } else {
            view.resignFirstResponder()
            self.onFinish?()
        }

        return true
    }
}

extension TextFieldChain : UITextFieldDelegate {
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return self.shouldReturn(view: textField)
    }
}

extension TextFieldChain : UITextViewDelegate {
    fileprivate func scrollToCursor(textView: UITextView) {
        if let scrollView = self.parentScrollView {
            if let textRange = textView.selectedTextRange {
                let textRect = textView.caretRect(for: textRange.start)
                let textRectInScrollView = scrollView.convert(textRect, from: textView)
                scrollView.scrollRectToVisible(textRectInScrollView, animated: true)
            }
        }
    }

    public func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if let d = self.delegates[textView] as? UITextViewDelegate {
            _ = d.textView?(textView, shouldChangeTextIn: range, replacementText: text)
        }
        //         if NSString(string: textView.text).replacingCharacters(in: range, with: text).hasSuffix("\n\n") {
        //            self.shouldReturn(view: textView)
        //        }
        return true
    }

    public func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        if let d = self.delegates[textView] as? UITextViewDelegate {
            _ = d.textViewShouldEndEditing?(textView)
        }
        return self.shouldReturn(view: textView)
    }

    public func textViewDidChange(_ textView: UITextView) {
        self.scrollToCursor(textView: textView)
        if let d = self.delegates[textView] as? UITextViewDelegate {
            d.textViewDidChange?(textView)
        }
    }
}
