//
//  SmartImageView.swift
//  iOS Helpers
//
//  Created by Dmitry Zganyaiko on 27/10/2018.
//  Copyright © 2018 Dmitry Zganyaiko. All rights reserved.
//

import Foundation
import UIKit

public class SmartImageView : UIImageView {
    private var _loadIndicator: UIActivityIndicatorView?

    private var loadIndicator: UIActivityIndicatorView {
        if let v = self._loadIndicator {
            return v
        }

        let v = UIActivityIndicatorView(style: .white)
        v.hidesWhenStopped = true
        v.autoresizingMask = [.flexibleLeftMargin, .flexibleBottomMargin, .flexibleRightMargin, .flexibleTopMargin]

        var f = v.frame
        f.origin = CGPoint(x: self.bounds.width * 0.5 - f.width * 0.5,
                           y: self.bounds.height * 0.5 - f.height * 0.5)
        v.frame = f

        self.addSubview(v)

        self._loadIndicator = v
        return v
    }

    public func loadImageFrom(url: URL?, fallback: UIImage? = nil, callback: (() -> Void)? = nil) {
        self.image = nil
        self.loadIndicator.startAnimating()

        if let url = url {
            ImageCache.shared.imageFor(url: url).done({ image in
                self.loadIndicator.stopAnimating()
                self.image = image
            }).catch({ (error) in
                NSLog("Can't load image from \(url): \(error)")

                self.loadIndicator.stopAnimating()
                self.image = fallback
            }).finally {
                callback?()
            }
        } else {
            self.loadIndicator.stopAnimating()
            self.image = fallback
        }
    }
}
