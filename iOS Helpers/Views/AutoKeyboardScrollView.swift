//
//  AutoKeyboardScrollView.swift
//  iOS Helpers
//
//  Created by Dmitry Zganyaiko on 27/10/2018.
//  Copyright © 2018 Dmitry Zganyaiko. All rights reserved.
//

import Foundation
import UIKit

public class AutoKeyboardScrollView : UIScrollView {
    private var originalInset = UIEdgeInsets.zero
    private var textFieldChain: TextFieldChain?

    public override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }

    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    public func createTextFieldChain(_ textFields: [UIView?], onFinish: (() -> Void)?) {
        self.textFieldChain = TextFieldChain(textFields: textFields, onFinish: onFinish)
        self.textFieldChain?.parentScrollView = self
    }

    private func commonInit() {
        self.originalInset = self.contentInset

        NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillShowNotification, object: nil, queue: nil) { [weak self] (n) in
            guard let self = self else {
                return
            }

            var inset = self.originalInset
            let kbdHeight = (n.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as! CGRect).height
            var bottomDelta: CGFloat = 0
            if let window = UIApplication.shared.windows.first {
                let rectInWindow = window.convert(self.frame, from: self.superview)
                bottomDelta = window.bounds.height - rectInWindow.origin.y - rectInWindow.height
            }
            inset.bottom += kbdHeight - bottomDelta
            self.contentInset = inset
        }

        NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillHideNotification, object: nil, queue: nil) { [weak self] (n) in
            guard let self = self else {
                return
            }
            self.contentInset = self.originalInset
        }
    }
}
