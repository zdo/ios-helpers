//
//  iOS_Helpers.h
//  iOS Helpers
//
//  Created by Dmitry Zganyaiko on 27/10/2018.
//  Copyright © 2018 Dmitry Zganyaiko. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for iOS_Helpers.
FOUNDATION_EXPORT double iOS_HelpersVersionNumber;

//! Project version string for iOS_Helpers.
FOUNDATION_EXPORT const unsigned char iOS_HelpersVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <iOS_Helpers/PublicHeader.h>


