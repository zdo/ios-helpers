//
//  Helper+Data.swift
//  iOS Helpers
//
//  Created by Dmitry Zganyaiko on 27/10/2018.
//  Copyright © 2018 Dmitry Zganyaiko. All rights reserved.
//

import Foundation

public protocol DataParseable {
    init(data: [String: Any]) throws
    func from(data: [String: Any]) throws
    func toData() -> [String: Any]
}

public enum DataParseError : LocalizedError {
    case invalidValueForKey(String)

    public var localizedDescription: String {
        switch self {
        case .invalidValueForKey(let key):
            return "Invalid value for key \(key)"
        }
    }

    public var errorDescription: String? {
        return self.localizedDescription
    }
}

extension Dictionary where Key == String {
    public func get<T>(_ key: String) throws -> T {
        if let v: T = self.getSafe(key) {
            return v
        } else {
            throw DataParseError.invalidValueForKey(key)
        }
    }

    public func getSafe<T>(_ key: String) -> T? {
        if let v = self[key] as? T {
            return v
        } else {
            return nil
        }
    }

    public mutating func setIfNotNil(_ key: String, _ value: Value?) {
        if let v = value {
            self[key] = v
        }
    }
}

extension Array where Element : DataParseable {
    public func toData() -> [[String: Any]] {
        return self.map { $0.toData() }
    }

    public mutating func from(data: [[String: Any]]) throws {
        self.removeAll()

        for v in data {
            let el = try Element(data: v)
            self.append(el)
        }
    }
}
