//
//  Helper+Date.swift
//  iOS Helpers
//
//  Created by Dmitry Zganyaiko on 27/10/2018.
//  Copyright © 2018 Dmitry Zganyaiko. All rights reserved.
//

import Foundation

extension Date {
    public func convertToTimeZone(initTimeZone: TimeZone, timeZone: TimeZone) -> Date {
        let delta = TimeInterval(timeZone.secondsFromGMT() - initTimeZone.secondsFromGMT())
        return addingTimeInterval(delta)
    }

    public var weekdayMonday0: Int {
        let dc = Calendar.gregorian.dateComponents([.weekday], from: self)
        return dc.weekdayMonday0!
    }
}

extension DateComponents {
    public var weekdayMonday0: Int? {
        guard let v = self.weekday else {
            return nil
        }

        var result = v - 2
        if result == -1 {
            result = 6
        }
        return result
    }
}

extension Calendar {
    public static var gregorian: Calendar {
        return Calendar(identifier: .gregorian)
    }
}
