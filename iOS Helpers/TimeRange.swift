//
//  TimeRange.swift
//  iOS Helpers
//
//  Created by Dmitry Zganyaiko on 12/11/2018.
//  Copyright © 2018 Dmitry Zganyaiko. All rights reserved.
//

import Foundation

public struct Time: Comparable {
    public var hour: Int = 0 {
        didSet {
            self.hour = max(min(self.hour % 24, 23), 0)
        }
    }
    public var minute: Int = 0 {
        didSet {
            self.minute = max(min(self.minute, 59), 0)
        }
    }

    public mutating func from(string: String) {
        let chars = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]

        let c = string.components(separatedBy: ":")
        if c.count == 2 {
            self.hour = Int(c[0]) ?? 0
            self.minute = Int(c[1].filter({ c -> Bool in
                return chars.contains(String(c))
            })) ?? 0
        } else {
            self.hour = Int(string) ?? 0
            self.minute = 0
        }

        let lc = string.lowercased()
        if lc.hasSuffix("pm") || lc.hasSuffix("p") {
            if self.hour != 12 {
                self.hour += 12
            }
        }
    }

    public mutating func from(double: Double) {
        self.hour = Int(floor(double))
        let doubleClamped = max(min(fmod(double, 24), 23), 0)
        self.minute = Int((doubleClamped - Double(self.hour)) * 60)
    }

    public init(hour: Int, minute: Int) {
        self.hour = hour
        self.minute = minute
    }

    public init(string: String) {
        self.from(string: string)
    }

    public init(double: Double) {
        self.from(double: double)
    }

    public init(dateComponents: DateComponents) {
        self.hour = dateComponents.hour ?? 0
        self.minute = dateComponents.minute ?? 0
    }

    public var minutesValues: Int {
        return self.minute + self.hour * 60
    }

    public static func < (lhs: Time, rhs: Time) -> Bool {
        return lhs.minutesValues < rhs.minutesValues
    }

    public static func <= (lhs: Time, rhs: Time) -> Bool {
        return lhs.minutesValues <= rhs.minutesValues
    }

    public static func >= (lhs: Time, rhs: Time) -> Bool {
        return lhs.minutesValues >= rhs.minutesValues
    }

    public static func > (lhs: Time, rhs: Time) -> Bool {
        return lhs.minutesValues > rhs.minutesValues
    }

    public var timeString: String {
        return self.timeString()
    }

    public func timeString(dateFormat: String = "h:mma") -> String {
        let fmt = DateFormatter()
        fmt.dateFormat = dateFormat

        var dc = Calendar.gregorian.dateComponents([.hour, .minute], from: Date())
        dc.hour = self.hour
        dc.minute = self.minute
        let d0 = Calendar.gregorian.date(from: dc)!
        let s0 = fmt.string(from: d0)
        return s0
    }
}

public struct TimeRange {
    public var from, to: Time?

    public mutating func from(strings: [String?]) {
        guard strings.count >= 1 else {
            print("[ERROR] Strings count must be >= 1")
            return
        }

        if let s = strings[0] {
            self.from = Time(string: s)
        } else {
            self.from = nil
        }

        if strings.count >= 2, let s = strings[1] {
            self.to = Time(string: s)
        } else {
            self.to = nil
        }
    }

    public init(strings: [String?]) {
        self.from(strings: strings)
    }
}

public struct WeekTimeRange {
    public var list: [TimeRange?]

    public init() {
        self.list = [TimeRange?](repeating: nil, count: 7)
    }

    public mutating func from(strings: [[String?]?]) {
        guard strings.count == 7 else {
            print("[ERROR] Strings count must be 7")
            return
        }

        for i in 0 ..< 7 {
            if let s = strings[i] {
                self.list[i] = TimeRange(strings: s)
            } else {
                self.list[i] = nil
            }
        }
    }

    public mutating func from(jsonString: String) {
        do {
            let jsonData = try JSONSerialization.jsonObject(with: jsonString.data(using: .utf8)!, options: [])
            guard let strings = jsonData as? [[String?]?] else {
                print("[ERROR] Can't convert json data to [[String]]: \(jsonData)")
                return
            }
            self.from(strings: strings)
        } catch let exc {
            print("[ERROR] Can't parse json string \(jsonString): \(exc)")
        }
    }
}
