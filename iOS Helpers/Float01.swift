//
//  Float01.swift
//  iOS Helpers
//
//  Created by Dmitry Zganyaiko on 30/10/2018.
//  Copyright © 2018 Dmitry Zganyaiko. All rights reserved.
//

import Foundation

public struct Float01 {
    public static let zero = Float01(0)

    private var _value: Float = 0

    public var value: Float {
        get {
            return _value
        }
        set {
            _value = max(min(newValue, 1.0), 0.0)
        }
    }

    public init(_ v: Float) {
        self.value = v
    }
}
