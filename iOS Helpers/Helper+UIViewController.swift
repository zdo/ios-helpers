//
//  Helper+UIViewController.swift
//  iOS Helpers
//
//  Created by Dmitry Zganyaiko on 27/10/2018.
//  Copyright © 2018 Dmitry Zganyaiko. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    class func _createFromXib<T>(_ : T.Type) -> T {
        let fileName = String(describing: self)
        print("Create VC from xib: self=\(self) fileName=\(fileName)")
        let storyboard = UIStoryboard.init(name: fileName, bundle: nil)
        let vc = storyboard.instantiateInitialViewController()
        return vc as! T
    }

    public class func createFromXib() -> Self {
        return self._createFromXib(self)
    }

    public func messageBox(_ text: String, title: String? = nil) {
        print("MessageBox: \(text)")
        let alert = UIAlertController(title: title, message: text, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

    public func messageBoxError(_ text: String, title: String? = "Error") {
        self.messageBox(text, title: title)
    }
}



extension URLRequest {
    public func curl(pretty:Bool = false) -> String {

        var data : String = ""
        let complement = pretty ? "\\\n" : ""
        let method = "-X \(self.httpMethod ?? "GET") \(complement)"
        let url = "\"" + (self.url?.absoluteString ?? "") + "\""

        var header = ""

        if let httpHeaders = self.allHTTPHeaderFields, httpHeaders.keys.count > 0 {
            for (key,value) in httpHeaders {
                header += "-H \"\(key): \(value)\" \(complement)"
            }
        }

        if let bodyData = self.httpBody, let bodyString = String(data:bodyData, encoding:.utf8) {
            data = "-d \"\(bodyString)\" \(complement)"
        }

        let command = "curl -i " + complement + method + header + data + url

        return command
    }

}
