//
//  Helper+Dict.swift
//  iOS Helpers
//
//  Created by Dmitry Zganyaiko on 27/10/2018.
//  Copyright © 2018 Dmitry Zganyaiko. All rights reserved.
//

import Foundation

extension Dictionary {
    public static var jsonString: String? {
        do {
            let data = try JSONSerialization.data(withJSONObject: self, options: [])
            let str = String(data: data, encoding: .utf8)
            return str
        } catch let error {
            NSLog("Can't convert dictionary \(self) to JSON: \(error)")
            return nil
        }
    }
}
