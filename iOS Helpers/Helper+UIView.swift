//
//  Helper+UIView.swift
//  iOS Helpers
//
//  Created by Dmitry Zganyaiko on 27/10/2018.
//  Copyright © 2018 Dmitry Zganyaiko. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    static private var cachedXibs = [String: UINib]()

    public func loadView(xib: String) {
        var nib: UINib! = UIView.cachedXibs[xib]
        if nib == nil {
            let bundle = Bundle(for: type(of: self))
            nib = UINib(nibName: xib, bundle: bundle)
            UIView.cachedXibs[xib] = nib
        }
        let v = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        self.frame = v.bounds

        self.addSubview(v)
    }
}
