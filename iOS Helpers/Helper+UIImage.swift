//
//  Helper+UIImage.swift
//  iOS Helpers
//
//  Created by Dmitry Zganyaiko on 27/10/2018.
//  Copyright © 2018 Dmitry Zganyaiko. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
    public func imageWithInsets(insetDimen: CGFloat) -> UIImage {
        return imageWithInset(insets: UIEdgeInsets(top: insetDimen, left: insetDimen, bottom: insetDimen, right: insetDimen))
    }

    public func imageWithInset(insets: UIEdgeInsets) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(
            CGSize(width: self.size.width + insets.left + insets.right,
                   height: self.size.height + insets.top + insets.bottom), false, self.scale)
        let origin = CGPoint(x: insets.left, y: insets.top)
        self.draw(at: origin)
        let imageWithInsets = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return imageWithInsets!
    }

    public func imageWith(size: CGSize) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, self.scale)
        self.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        let resultImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return resultImage!
    }

    public func imageWith(maxSize: CGFloat) -> UIImage {
        var newSize = self.size
        if self.size.width > self.size.height {
            newSize.width = maxSize
            newSize.height = newSize.width / self.size.width * self.size.height
        } else {
            newSize.height = maxSize
            newSize.width = newSize.height / self.size.height * self.size.width
        }
        return self.imageWith(size: newSize)
    }

    public func imageWith(height: CGFloat) -> UIImage {
        var newSize = self.size
        newSize.height = height
        newSize.width = self.size.width / self.size.height * height
        return self.imageWith(size: newSize)
    }
}
