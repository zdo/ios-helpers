//
//  ImageCache.swift
//  iOS Helpers
//
//  Created by Dmitry Zganyaiko on 27/10/2018.
//  Copyright © 2018 Dmitry Zganyaiko. All rights reserved.
//

import Foundation
import UIKit
import PromiseKit
import SwiftHash
import Alamofire

public enum ImageCacheError : Error {
    case loadImageError
    case downloadImageError
}

public class ImageCache {
    public static let shared = ImageCache()

    internal struct ImageEntry {
        var localUrl: URL
        var image: UIImage
    }

    private var queue = DispatchQueue(label: "ImageCache")
    private var maxLoadedImages = 50
    private var loadedImages = [ImageEntry]()
    private var downloadingUrlPromises = [URL: [Resolver<UIImage>]]()

    public func imageFor(url: URL) -> Promise<UIImage> {
        return Promise { resolver in
            self.queue.async {
                let localUrl = self.localUrlFor(remoteUrl: url)
                if let index = self.loadedImages.firstIndex(where: { $0.localUrl == localUrl }) {
                    // Image is already loaded.
                    let imageEntry = self.loadedImages[index]
                    self.loadedImages.remove(at: index)
                    self.loadedImages.insert(imageEntry, at: 0)

                    resolver.fulfill(imageEntry.image)
                } else if FileManager.default.fileExists(atPath: localUrl.path) {
                    // Image downloaded but not loaded.
                    if let image = UIImage(contentsOfFile: localUrl.path) {
                        let imageEntry = ImageEntry(localUrl: localUrl, image: image)
                        self.loadedImages.insert(imageEntry, at: 0)

                        resolver.fulfill(imageEntry.image)
                    } else {
                        try? FileManager.default.removeItem(at: localUrl)
                        resolver.reject(ImageCacheError.loadImageError)
                    }
                } else {
                    if self.downloadingUrlPromises[url] == nil {
                        self.downloadingUrlPromises[url] = [Resolver<UIImage>]()
                    }
                    self.downloadingUrlPromises[url]!.append(resolver)

                    Alamofire.request(url).responseData { response in
                        self.queue.async {
                            guard let resolvers = self.downloadingUrlPromises[url] else {
                                return
                            }

                            guard let imageData = response.data else {
                                for r in resolvers {
                                    r.reject(ImageCacheError.downloadImageError)
                                }
                                return
                            }

                            guard let image = UIImage(data: imageData) else {
                                for r in resolvers {
                                    r.reject(ImageCacheError.loadImageError)
                                }
                                return
                            }

                            do {
                                try imageData.write(to: localUrl)
                            } catch let writeError {
                                NSLog("[ERROR] Can't save image to local cache: \(writeError)")
                            }

                            let imageEntry = ImageEntry(localUrl: localUrl, image: image)
                            self.loadedImages.insert(imageEntry, at: 0)

                            for r in resolvers {
                                r.fulfill(image)
                            }
                            self.downloadingUrlPromises.removeValue(forKey: url)
                        }
                    }
                }
            }
        }
    }

    private func localUrlFor(remoteUrl: URL) -> URL {
        var ext = remoteUrl.pathExtension
        if ext.count == 0 {
            ext = "jpg"
        }
        let fileName = "cache_\(MD5(remoteUrl.absoluteString)).\(ext)"
        let dir = NSTemporaryDirectory()
        let path = dir + "/" + fileName
        let url = URL(fileURLWithPath: path, isDirectory: false)
        return url
    }
}
