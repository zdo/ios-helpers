Pod::Spec.new do |s|
  s.name         = "iOSHelpers"
  s.version      = "1.16"
  s.summary      = "Helpers"
  s.platform = :ios
  s.ios.deployment_target = '11.0'
  s.requires_arc = true

  s.description  = <<-DESC
Set of iOS helpers
                   DESC

  s.homepage     = "https://gitlab.com/zdo/ios-helpers"
  s.license      = { :type => "MIT", :file => "LICENSE" }

  s.author             = { "Dmitry Zganyaiko" => "zdo.str@gmail.com" }
  s.source       = { :git => "https://gitlab.com/zdo/ios-helpers.git", :tag => "#{s.version}" }

  s.source_files  = "iOS Helpers/**/*.swift"
  s.swift_version = "4.2"

  s.framework = "UIKit"
  s.dependency 'Alamofire'
  s.dependency 'PromiseKit'
  s.dependency 'SwiftHash'
end
